.PONY: i t it

COMPOSER = $(shell which composer)
PHPUNIT = vendor/bin/phpunit
PHPCS = vendor/bin/php-cs-fixer
PHP = $(shell which php)

it: i t

i:
	$(COMPOSER) install -o -a

t:
	$(PHPCS) fix -v
	$(PHP) vendor/bin/phpstan analyze -l 3 -c phpstan.neon src
#	$(PHP) vendor/bin/phpstan analyze -l 1 -c phpstan.neon tests
