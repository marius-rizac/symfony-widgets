Add widgets functionality in layout


### Installation

```bash
composer require kisphp/symfony-widgets
```

### Add to AppKernel.php file

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            ...
            new WidgetsBundle\WidgetsBundle(),
        );

        ...

        return $bundles;
    }

    ...
}
```


### Add configuration data `app/config/config.yml`

```yaml
widgets:
    boxFormsNamespaces:
        - AcmeBundle/Form/BoxForms
```

### Register Twig extension in Bundle's `services.yml`

```yaml
services:
    twig.widgets_extensions:
        class: WidgetsBundle\Twig\WidgetsExtensions
        public: true
        arguments: [@widgets_service]
        tags:
            - { name: twig.extension }
```
