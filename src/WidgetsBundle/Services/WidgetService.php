<?php

namespace WidgetsBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use WidgetsBundle\Entity\Repository\WidgetsRepository;
use WidgetsBundle\Entity\WidgetsEntity;

class WidgetService
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var Container
     */
    protected $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->twig = $container->get('twig');
    }

    /**
     * @param int $idZone
     *
     * @return null|string
     */
    public function drawZone($idZone)
    {
        $content = $this->getZoneContent($idZone);

        if (true === empty($content)) {
            return null;
        }

        $boxContent = '';

        foreach ($content as $box) {
            $boxContent .= $this->getTemplateContent($box);
        }

        return $boxContent;
    }

    /**
     * @param int $idZone
     *
     * @return array
     */
    protected function getZoneContent($idZone)
    {
        /** @var WidgetsRepository $repo */
        $repo = $this->em->getRepository('WidgetsBundle:WidgetsEntity');

        return $repo->getActiveWidgets($idZone);
    }

    /**
     * @param WidgetsEntity $box
     *
     * @return string
     */
    protected function getTemplateContent(WidgetsEntity $box)
    {
        // @todo render controller ?
//        $boxData = $box->getBoxData();
//        if (isset($boxData[SidebarIncludeBoxForm::FIELD_TEMPLATE]) && array_key_exists($boxData[SidebarIncludeBoxForm::FIELD_TEMPLATE], SidebarIncludedTemplates::getControllersList())) {
//            dump($box->getBoxData());
//        }

        $formClass = $box->getForm();
        $form = new $formClass();

        return $this->twig->render($box->getTemplate(), $form->populateTemplate($this->container, $box));
    }
}
