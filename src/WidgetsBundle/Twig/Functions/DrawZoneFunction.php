<?php

namespace WidgetsBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;
use WidgetsBundle\Services\WidgetService;

class DrawZoneFunction extends AbstractTwigFunction
{
    /**
     * @var WidgetService
     */
    protected $widgetService;

    /**
     * @param WidgetService $service
     */
    public function __construct(WidgetService $service)
    {
        $this->widgetService = $service;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getFunctionName()
    {
        return 'drawZone';
    }

    /**
     * @return \Closure
     */
    protected function getFunction()
    {
        $widgetService = $this->widgetService;

        return function ($idZone) use ($widgetService) {
            return $widgetService->drawZone($idZone);
        };
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [
            'is_safe' => ['html'],
        ];
    }
}
