<?php

namespace WidgetsBundle\Twig;

use WidgetsBundle\Services\WidgetService;
use WidgetsBundle\Twig\Functions\DrawZoneFunction;

class WidgetsExtensions extends \Twig_Extension
{
    /**
     * @var WidgetService
     */
    protected $widgetService;

    /**
     * WidgetsExtensions constructor.
     *
     * @param WidgetService $widgetService
     */
    public function __construct(WidgetService $widgetService)
    {
        $this->widgetService = $widgetService;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new DrawZoneFunction($this->widgetService),
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'widgets_extensions';
    }
}
