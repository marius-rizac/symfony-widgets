<?php

namespace WidgetsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Kisphp\Utils\Status;
use Kisphp\Entity\ToggleableInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="widgets_zone")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="WidgetsBundle\Entity\Repository\WidgetsZoneRepository")
 */
class WidgetsZoneEntity implements ToggleableInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"default": 2})
     */
    protected $status = Status::ACTIVE;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registered;

    /**
     * @var WidgetsEntity|ArrayCollection
     * @ORM\OneToMany(targetEntity="WidgetsEntity", mappedBy="zone")
     */
    protected $widgets;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->getTitle();
    }

    public function __construct()
    {
        $this->widgets = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDatetime()
    {
        $this->setRegistered(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * @param \DateTime $registered
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }

    /**
     * @return WidgetsEntity
     */
    public function getWidgets()
    {
        return $this->widgets;
    }

    /**
     * @param WidgetsEntity $widgets
     */
    public function setWidgets($widgets)
    {
        $this->widgets = $widgets;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
