<?php

namespace WidgetsBundle\Entity;

use Kisphp\Entity\FileInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="widgets_attached")
 * @ORM\Entity(repositoryClass="WidgetsBundle\Entity\Repository\WidgetsAttachedRepository")
 */
class WidgetsAttached implements FileInterface
{
    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $id_widget;

    /**
     * @ORM\ManyToOne(targetEntity="WidgetsEntity", inversedBy="images")
     * @ORM\JoinColumn(name="id_widget", referencedColumnName="id")
     */
    protected $widget;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $directory;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $filename;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdWidget()
    {
        return $this->id_widget;
    }

    /**
     * @param mixed $id_widget
     */
    public function setIdWidget($id_widget)
    {
        $this->id_widget = $id_widget;
    }

    /**
     * @return mixed
     */
    public function getWidget()
    {
        return $this->widget;
    }

    /**
     * @param mixed $widget
     */
    public function setWidget($widget)
    {
        $this->widget = $widget;
    }

    /**
     * @return mixed
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param mixed $directory
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}
