<?php

namespace WidgetsBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Kisphp\Utils\Status;

class WidgetsRepository extends EntityRepository
{
    /**
     * @param int $idZone
     *
     * @return array
     */
    public function getActiveWidgets($idZone)
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.status = :status AND a.id_zone = :zone')
            ->orderBy('a.priority', 'DESC')
            ->setParameter('status', Status::ACTIVE)
            ->setParameter('zone', $idZone)
            ->getQuery()
        ;

        return $query->getResult();
    }

    /**
     * list widgets in admin on zone page
     *
     * @param int $idZone
     *
     * @return array
     */
    public function getWidgetsForAdmin($idZone)
    {
        $query = $this->createQueryBuilder('a')
            ->andWhere('a.status > :status AND a.id_zone = :zone')
            ->orderBy('a.priority', 'DESC')
            ->setParameter('status', Status::DELETED)
            ->setParameter('zone', $idZone)
            ->getQuery()
        ;

        return $query->getResult();
    }
}
