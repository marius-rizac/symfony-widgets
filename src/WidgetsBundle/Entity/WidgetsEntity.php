<?php

namespace WidgetsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Kisphp\Utils\Status;
use Kisphp\Entity\ToggleableInterface;
use WidgetsBundle\Entity\Decorator\WidgetEntityDecorator;
use WidgetsBundle\Form\AbstractWidgetForm;

/**
 * @ORM\Entity
 * @ORM\Table(name="widgets")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="WidgetsBundle\Entity\Repository\WidgetsRepository")
 */
class WidgetsEntity implements ToggleableInterface
{
    const DEFAULT_PRIORITY = 0;

    /**
     * @ORM\Column(type="integer", options={"unsigned": true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="integer", options={"default": 2})
     */
    protected $status = Status::ACTIVE;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"unsigned": true})
     */
    protected $id_zone;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $registered;

    /**
     * @var int
     * @ORM\Column(type="integer", options={"unsigned": true})
     */
    protected $priority = self::DEFAULT_PRIORITY;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $form;

    /**
     * @var WidgetsZoneEntity
     *
     * @ORM\ManyToOne(targetEntity="WidgetsZoneEntity", inversedBy="widgets")
     * @ORM\JoinColumn(name="id_zone", referencedColumnName="id")
     */
    protected $zone;

    /**
     * @ORM\OneToMany(targetEntity="WidgetsAttached", mappedBy="widget")
     */
    protected $images;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $box_data;

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     */
    public function updateModifiedDatetime()
    {
        $this->setRegistered(new \DateTime());
    }

    /**
     * @return WidgetEntityDecorator
     */
    public function getDecorator()
    {
        return new WidgetEntityDecorator($this);
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        $formName = $this->getForm();

        /** @var AbstractWidgetForm $form */
        $form = new $formName();
        $form->setEntity($this);
        $template = $form->getTemplate();

        if (true === empty($template)) {
            $template = 'WidgetsBundle:Templates:empty.html.twig';
        }

        return $template;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return (int) $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getIdZone()
    {
        return $this->id_zone;
    }

    /**
     * @param mixed $id_zone
     */
    public function setIdZone($id_zone)
    {
        $this->id_zone = $id_zone;
    }

    /**
     * @return \DateTime
     */
    public function getRegistered()
    {
        return $this->registered;
    }

    /**
     * @param \DateTime $registered
     */
    public function setRegistered($registered)
    {
        $this->registered = $registered;
    }

    /**
     * @return WidgetsZoneEntity
     */
    public function getZone()
    {
        return $this->zone;
    }

    /**
     * @param WidgetsZoneEntity $zone
     */
    public function setZone(WidgetsZoneEntity $zone)
    {
        $this->zone = $zone;
        $this->setIdZone($zone->getId());
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getBoxData()
    {
        if (null === $this->box_data) {
            return [];
        }

        return unserialize($this->box_data);
    }

    /**
     * @param array $box_data
     */
    public function setBoxData(array $box_data)
    {
        $this->box_data = serialize($box_data);
    }

    /**
     * @return mixed
     */
    public function getForm()
    {
        return $this->form;
    }

    /**
     * @param mixed $form
     */
    public function setForm($form)
    {
        $chunks = explode('src', $form);
        $form = end($chunks);

        $this->form = $form;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
    }
}
